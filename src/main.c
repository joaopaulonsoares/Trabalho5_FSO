#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int quantMaxArq=0;
int contaQuantidade=0;
char wordCompare[20];

int verifyWords(char *main_string){
  int i=0, j=0, encontra_char=0, existe=0;
  int num_chars = strlen(wordCompare);

   while (main_string[i]!='\0'){
      if (main_string[i] == wordCompare[j]){
         i++;
         j++;
        encontra_char = 1;
         if (j == num_chars) {
            j = 0;
            existe = 1;
          }
      }
      else{
        if (encontra_char == 1) {
          j = 0;
          encontra_char = 0;
        }
        else {
          i++;
        }
      }
   }

   if(existe){
    return 2;
  }else{
    return 1;
  }

}

void readFile(const char *name,char *dirName,char *fileName){
    FILE *file;
    char c;
    char dir[50];
    char str[50];
    int qtChar=0;

    snprintf( dir, 50, "%s%s",name,fileName);
    printf("%d./%s --\n",contaQuantidade+1,dir);

    if (!(file = fopen (dir, "r"))) {
      printf("Desculpe, o arquivo nao pode ser aberto.");
      exit(0);
    }
    printf("  ");

    while (fscanf(file, "%c", &c)!=EOF){
      printf("%c",c);
      qtChar++;
      if(qtChar == 30){
        break;
      }
    }

    printf("\n" );

    fclose(file);
}


//Source: https://stackoverflow.com/questions/8436841/how-to-recursively-list-directories-in-c-on-linux
void listdir(const char *name, int level,char *dirName){
    DIR *dir;
    struct dirent *entry;

    if (!(dir = opendir(name)))
        return;
    if (!(entry = readdir(dir)))
        return;

    do {
        if (entry->d_type == DT_DIR) {
            char path[1024];
            int len = snprintf(path, sizeof(path)-1, "%s%s/", name, entry->d_name);

            path[len] = 0;
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            //printf("%*s[%s%s]\n", level*2, "",name,entry->d_name);
            listdir(path, level + 1,entry->d_name);
        }
        else{
            // printf("Name: %s\n",name );
            // printf("%*s- (%s)\n", level*2, "", entry->d_name);
            //printf("Valor:\n\t");Do
            // printf("\nENTRY NAME: %s\n",entry->d_name );

            if(verifyWords(entry->d_name)==2){
              readFile(name,dirName,entry->d_name);
              contaQuantidade++;
            }

            if(contaQuantidade == quantMaxArq){
              exit(0);
            }

        }

    } while (entry = readdir(dir));
    closedir(dir);
}

int main(int argc, char* argv[]){
    char dirName[50];

    strncpy(wordCompare,argv[2],sizeof(argv[2]));
    quantMaxArq = atoi(argv[3]);
    snprintf( dirName, 50, "%s/",argv[1]);

    listdir(dirName, 0,"");

    return 0;
}
