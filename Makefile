INCFOLDER := inc/
SRCFOLDER := src/
CC := gcc
CFLAGS :=
SRCFILES := $(wildcard src/*.c)
all: $(SRCFILES:src/%.c=%.o)
	$(CC) $(CFLAGS) *.o -o buscador
	rm -rf *.o
%.o: src/%.c
	$(CC) $(CFLAGS) -c $< -o $@ -I./inc
.PHONY: clean
clean:
	rm -rf main
